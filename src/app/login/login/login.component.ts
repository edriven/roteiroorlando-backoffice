import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../http/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public usuario;
  public senha;
  public exception;

  constructor(
    private router: Router,
    private loginService: LoginService
  ) { }

  ngOnInit() {
    this.loginService.logout();
  }

  login(){
    this.exception = null;
    this.loginService.login(this.usuario, this.senha).then(usuario =>{
        this.loginService.isAdmin(usuario.user.uid).then(isAdmin =>{
            if(isAdmin){
                this.router.navigate(['/dashboard']);
            } else {
                this.exception = 'Usuário não autorizado.'
                this.loginService.logout();
            }
        });
    })
    .catch(exception =>{
        this.exception = 'E-mail ou senha incorretos.'
    });
  }
}
