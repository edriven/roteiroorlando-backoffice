import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource } from '@angular/material';
import { VoucherService } from '../../http/voucher.service';
import { UsuarioService } from '../../http/usuario.service';

@Component({
  selector: 'app-grid-voucher',
  templateUrl: './grid-voucher.component.html',
  styleUrls: ['./grid-voucher.component.css']
})
export class GridVoucherComponent implements OnInit {
  public colunas = ['name'];
  public usuarioVoucher = new MatTableDataSource();

  constructor(
    private voucherService: VoucherService,
    private usuarioService: UsuarioService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    // this.voucherService.listar().subscribe(vouchers =>{
    //   console.log('vouchers', vouchers);
    //   this.usuarioVoucher = new MatTableDataSource(vouchers);
      
    //   vouchers.forEach(voucher => {
    //     this.categorias.add(voucher.category);
    //   });
    // });
    this.listarUsuarios();
  }

  listarUsuarios(filtro?){
    this.usuarioService.listar(filtro).subscribe(usuarios =>{
      this.usuarioVoucher = new MatTableDataSource(usuarios);
    });
  }

  selecionarLinha(linha){
    this.router.navigate(['cadastro/'+ linha.uid], {relativeTo: this.activatedRoute});
  }
}
