import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VoucherComponent } from './voucher/voucher.component';
import { CadastroVoucherComponent } from './cadastro-voucher/cadastro-voucher.component';

const routes: Routes = [
  {
    path: '',
    component: VoucherComponent,
  },
  {
    path: 'cadastro',
    component: CadastroVoucherComponent,
  },
  {
    path: 'cadastro/:id',
    component: CadastroVoucherComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VoucherRoutingModule { }
