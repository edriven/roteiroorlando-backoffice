import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource, MatSnackBarRef, SimpleSnackBar, MatSnackBar } from '@angular/material';
import { UsuarioService } from '../../http/usuario.service';
import { VoucherService } from '../../http/voucher.service';

@Component({
  selector: 'app-cadastro-voucher',
  templateUrl: './cadastro-voucher.component.html',
  styleUrls: ['./cadastro-voucher.component.css']
})
export class CadastroVoucherComponent implements OnInit {
  public id: any;
  public voucher: any = [];
  public usuarios: any =[];
  public vouchers = null;
  public arquivo = null;
  public colunas = ['category', 'remover'];
  private snackBarRef: MatSnackBarRef<SimpleSnackBar>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private usuarioService: UsuarioService,
    private voucherService: VoucherService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.voucher = {};

    this.route.params.subscribe(params =>{
      this.id = params['id'];

      if(this.id){
        this.voucherService.getVouchersByUserId(this.id).subscribe(vouchers =>{
          this.vouchers = vouchers.length > 0 ? new MatTableDataSource(vouchers) : null;
        });

        this.usuarioService.obterByUid(this.id).subscribe(usuario =>{
          this.usuarios.push(usuario[0]);
          this.voucher.userId = this.id;
        });
      } else {
        this.usuarioService.listar().subscribe(usuarios =>{
          this.usuarios = usuarios;
        });
      }
    });

  }

  delete(voucherDelete){
    this.snackBarRef = this.snackBar.open('Confirma exclusão do voucher?', 'Confirmar', {duration: 5000});

    this.snackBarRef.onAction().subscribe(() =>{
      this.voucherService.delete(voucherDelete).then(() =>{
        this.snackBar.open('Voucher removido com sucesso!');
      });
    });
  }

  salvar(){
    if(!this.arquivo){
      this.snackBar.open('Selecione um arquivo');
      return;
    }

    if(this.arquivo.name.split('.').pop().toLowerCase() != 'pdf'){
      this.voucher.type = 'image';
    } else {
      this.voucher.type = 'pdf';
    }

    console.log("this.voucher", this.voucher);
    this.voucherService.save(this.voucher, this.arquivo).then(() =>{
      this.snackBar.open('Voucher cadastrado com sucesso!');
      this.router.navigateByUrl('dashboard/voucher');
    });
  }
}
