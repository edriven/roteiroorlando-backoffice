import { NgModule } from '@angular/core';

import { VoucherRoutingModule } from './voucher-routing.module';
import { VoucherComponent } from './voucher/voucher.component';
import { SharedModule } from '../shared.module';
import { GridVoucherComponent } from './grid-voucher/grid-voucher.component';
import { CadastroVoucherComponent } from './cadastro-voucher/cadastro-voucher.component';

@NgModule({
  imports: [
    SharedModule,
    VoucherRoutingModule
  ],
  declarations: [VoucherComponent, GridVoucherComponent, CadastroVoucherComponent],
  exports: [SharedModule]
})
export class VoucherModule { }
