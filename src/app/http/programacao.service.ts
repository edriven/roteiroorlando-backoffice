import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProgramacaoService {

  constructor(
    private db: AngularFirestore
  ) { }

  listar(): Observable<any>{
    return this.db.collection('schedule').valueChanges();
  }
}
