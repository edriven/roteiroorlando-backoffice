import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireStorage } from 'angularfire2/storage';
import * as firebase from 'firebase';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VoucherService {

  constructor(
    private db: AngularFirestore,
    private storage: AngularFireStorage
  ) { }

  getVouchersByUserId(userId): Observable<any>{
    return this.db.collection('vouchers', ref =>{
      return ref.where('userId', '==', userId);
    }).valueChanges();
  }

  delete(voucher){
    this.storage.storage.refFromURL(voucher.url).delete();
    return  this.db.collection('vouchers').doc(voucher.id).delete();
  }

  save(voucher, arquivo): Promise<void>{
    return new Promise<void>(resolve =>{
      this.storage.ref('/'+ voucher.userId +'/'+ arquivo.name).put(arquivo).then(uploadTask =>{
        uploadTask.ref.getDownloadURL().then(downloadUrl =>{
          voucher.id = this.db.createId();
          voucher.createdAt = firebase.firestore.FieldValue.serverTimestamp();
          voucher.url = downloadUrl;
          
          this.db.collection('vouchers').doc(voucher.id).set(voucher).then(() =>{
            resolve();
          });
        });
      });
    });
  }
}
