import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private authState: any;

  constructor(
    private db: AngularFirestore,
    private firebaseAuth: AngularFireAuth
  ) {
    this.firebaseAuth.authState.subscribe((auth) => {
        this.authState = auth;
    });
  }

  login(username, password): Promise<any>{
    return new Promise<any>((resolve, reject) =>{
        this.firebaseAuth.auth.setPersistence('session').then(()=>{
            this.firebaseAuth.auth.signInWithEmailAndPassword(username, password).then(user =>{
                resolve(user);
            })
            .catch(exception =>{
                reject(exception);
            });
        });
    });
  }

  isAdmin(userId): Promise<Boolean>{
    return new Promise<any>((resolve, reject) =>{
        this.db.collection('admins').doc(userId).valueChanges().subscribe(data =>{
            resolve(data != null);
        });
    });
  }

  logout(): Promise<void>{
    return this.firebaseAuth.auth.signOut();
  }

  isLogged(): Boolean{
    return this.authState !== null;
  }
}
