import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChecklistService {

  constructor(
    private db: AngularFirestore
  ) { }

  listar(): Observable<any>{
    return this.db.collection('checklists').snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  obter(checklistId): Observable<any>{
    return this.db.collection('checklists').doc(checklistId).collection('todos').snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  update(checklistId, itens): Promise<any>{
    let collectionReference = this.db.firestore.collection("checklists").doc(checklistId).collection('todos');
    let batch = this.db.firestore.batch();
    
    itens.forEach(item => {
      if(item.id){
        batch.update(collectionReference.doc(item.id), item);
      } else {
        item.id = this.db.createId();
        batch.set(collectionReference.doc(item.id), item);
      }
    });

    return batch.commit();
    // this.db.firestore.collection("checklists").doc(checklistId).collection('todos').add(itens);
  }


}
