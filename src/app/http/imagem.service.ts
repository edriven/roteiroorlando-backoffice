import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ImagemService {

    constructor(
        private db: AngularFirestore,
        private storage: AngularFireStorage
    ) { }

    obter(id): Observable<any>{
        return this.db.collection('images').doc(id).valueChanges();
    }

    listar(): Observable<any>{
        return this.db.collection('images', ref =>{
            let query : firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
            query = query.where('active', '==', true)
            return query;
        }).snapshotChanges().pipe(
            map(actions => {
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const id = a.payload.doc.id;

                    return { id, ...data };
                });
            })
        );
    }

    upload(arquivo, id?): AngularFireUploadTask{
        if(!id){
            id = this.db.createId();
        }

        return this.storage.ref('/images/'+ id +'/'+ arquivo.name).put(arquivo);
    }

    update(imagem): Promise<any>{
        if(!imagem.id){
            imagem.id = this.db.createId();
            return this.db.collection('images').doc(imagem.id).set(imagem);
        } else {
            return this.db.collection('images').doc(imagem.id).update(imagem);
        }
    }
}
