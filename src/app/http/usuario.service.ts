import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    private db: AngularFirestore
  ) { }

  obter(id): Observable<any>{
    return this.db.collection('users').doc(id).valueChanges();
  }

  obterByUid(uid): Observable<any>{
    return this.db.collection('users', ref =>{
      let query : firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
      query = query.where('uid', '==', uid)
      return query;
    }).snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data: any = a.payload.doc.data();
          if(!data.id){
            data.id = a.payload.doc.id;
            return data;
          } else {
            const id = a.payload.doc.id;
            return { id, ...data };
          }
        });
      })
    );
  }

  listar(name?, isAssociated?): Observable<any>{
    return this.db.collection('users', ref =>{
      let query : firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
      if (name) { query = query.where('name', '==', name) };
      if (isAssociated) { query = query.where('isAssociated', '==', isAssociated) };
      query = query.orderBy('name');

      return query;
    }).snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data: any = a.payload.doc.data();
          if(!data.id){
            data.id = a.payload.doc.id;
            return data;
          } else {
            const id = a.payload.doc.id;
            return { id, ...data };
          }
        });
      })
    );
  }
}
