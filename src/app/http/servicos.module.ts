import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChecklistService } from './checklist.service';
import { ImagemService } from './imagem.service';
import { UsuarioService } from './usuario.service';
import { VoucherService } from './voucher.service';
import { WishlistService } from './wishlist.service';
import { ProgramacaoService } from './programacao.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class ServicosModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ServicosModule, 
      providers: [
        ChecklistService,
        ImagemService,
        UsuarioService,
        VoucherService,
        WishlistService,
        ProgramacaoService
      ]
    }; 
  }
}
