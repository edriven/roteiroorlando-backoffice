import { NgModule } from '@angular/core';

import { WishListRoutingModule } from './wish-list-routing.module';
import { WishListComponent } from './wish-list/wish-list.component';
import { SharedModule } from '../shared.module';

@NgModule({
  imports: [
    SharedModule,
    WishListRoutingModule
  ],
  declarations: [WishListComponent],
  exports: [SharedModule]
})
export class WishListModule { }
