import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImagemComponent } from './imagem/imagem.component';
import { CadastroImagemComponent } from './cadastro-imagem/cadastro-imagem.component';

const routes: Routes = [
  {
    path: '',
    component: ImagemComponent,
  },
  {
    path: 'cadastro/:id',
    component: CadastroImagemComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImagemRoutingModule { }
