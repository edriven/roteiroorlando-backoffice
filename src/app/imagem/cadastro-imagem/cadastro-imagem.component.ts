import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ImagemService } from '../../http/imagem.service';

@Component({
  selector: 'app-cadastro-imagem',
  templateUrl: './cadastro-imagem.component.html',
  styleUrls: ['./cadastro-imagem.component.css']
})
export class CadastroImagemComponent implements OnInit {
    public controleTela: any = {};
    public imagem: any = {};

    constructor(
        private imagemService: ImagemService,
        private snackBar: MatSnackBar,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.route.params.subscribe(params =>{
            this.imagem.id = params['id'];

            if(this.imagem.id){
                this.imagemService.obter(this.imagem.id).subscribe(imagem =>{
                    this.imagem = imagem;
                    this.imagem.id = params['id'];
                });
            }
        });
    }

    update(){
        let promisesUploads = [];

        if(this.controleTela.imagem1){
            promisesUploads[0] = this.imagemService.upload(this.controleTela.imagem1);
        }

        if(this.controleTela.imagem2){
            promisesUploads[1] = this.imagemService.upload(this.controleTela.imagem2);
        }

        if(this.controleTela.imagem3){
            promisesUploads[2] = this.imagemService.upload(this.controleTela.imagem3);
        }

        if(this.controleTela.imagem4){
            promisesUploads[3] = this.imagemService.upload(this.controleTela.imagem4);
        }

        Promise.all(promisesUploads).then(results =>{

            let promisesDownloadUrl = [];

            if(results[0]){
                this.imagem.image1StoragePath = results[0].metadata.fullPath;
                promisesDownloadUrl[0] = results[0].ref.getDownloadURL();
            }

            if(results[1]){
                this.imagem.image2StoragePath = results[1].metadata.fullPath;
                promisesDownloadUrl[1] = results[1].ref.getDownloadURL();
            }

            if(results[2]){
                this.imagem.image3StoragePath = results[2].metadata.fullPath;
                promisesDownloadUrl[2] = results[2].ref.getDownloadURL();
            }

            if(results[3]){
                this.imagem.image4StoragePath = results[3].metadata.fullPath;
                promisesDownloadUrl[3] = results[3].ref.getDownloadURL();
            }

            Promise.all(promisesDownloadUrl).then(downloadUrls =>{
                
                if(downloadUrls[0]){
                    this.imagem.image1DownloadURL = downloadUrls[0];
                }

                if(downloadUrls[1]){
                    this.imagem.image2DownloadURL = downloadUrls[1];
                }

                if(downloadUrls[2]){
                    this.imagem.image3DownloadURL = downloadUrls[2];
                }

                if(downloadUrls[3]){
                    this.imagem.image4DownloadURL = downloadUrls[3];
                }

                this.imagemService.update(this.imagem).then(() =>{
                    this.snackBar.open('Imagem atualizada com sucesso');
                    this.router.navigateByUrl('dashboard/imagem');
                });
            });
        });
    }

}
