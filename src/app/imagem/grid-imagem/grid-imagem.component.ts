import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource } from '@angular/material';
import { ImagemService } from '../../http/imagem.service';

@Component({
  selector: 'app-grid-imagem',
  templateUrl: './grid-imagem.component.html',
  styleUrls: ['./grid-imagem.component.css']
})
export class GridImagemComponent implements OnInit {
  public colunas = ['section'];
  public imagens = new MatTableDataSource();

  constructor(
    private imagemService: ImagemService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.imagemService.listar().subscribe(imagens =>{
      this.imagens = new MatTableDataSource(imagens)
    });
  }

  selecionarLinha(linha){
    this.router.navigate(['cadastro/'+ linha.id], {relativeTo: this.activatedRoute});
  }
}
