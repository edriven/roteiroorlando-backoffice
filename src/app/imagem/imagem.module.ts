import { NgModule } from '@angular/core';

import { ImagemRoutingModule } from './imagem-routing.module';
import { ImagemComponent } from './imagem/imagem.component';
import { SharedModule } from '../shared.module';
import { CadastroImagemComponent } from './cadastro-imagem/cadastro-imagem.component';
import { GridImagemComponent } from './grid-imagem/grid-imagem.component';

@NgModule({
  imports: [
    SharedModule,
    ImagemRoutingModule
  ],
  declarations: [ImagemComponent, CadastroImagemComponent, GridImagemComponent],
  exports: [SharedModule]
})
export class ImagemModule { }
