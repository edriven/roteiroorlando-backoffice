import { NgModule } from '@angular/core';

import { UsuarioRoutingModule } from './usuario-routing.module';
import { UsuarioComponent } from './usuario/usuario.component';
import { SharedModule } from '../shared.module';
import { GridUsuarioComponent } from './grid-usuario/grid-usuario.component';

@NgModule({
  imports: [
    SharedModule,
    UsuarioRoutingModule
  ],
  declarations: [UsuarioComponent, GridUsuarioComponent],
  exports: [SharedModule]
})
export class UsuarioModule { }
