import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { UsuarioService } from '../../http/usuario.service';

@Component({
  selector: 'app-grid-usuario',
  templateUrl: './grid-usuario.component.html',
  styleUrls: ['./grid-usuario.component.css']
})
export class GridUsuarioComponent implements OnInit {

  public colunas = ['nome', 'associado'];
  public usuarios = new MatTableDataSource();
  

  constructor(
    private usuarioService: UsuarioService
  ) { }

  ngOnInit() {
    this.listarUsuarios(null, true);
  }

  listarUsuarios(nome?, isAssociated?){
    this.usuarioService.listar(nome, isAssociated).subscribe(usuarios =>{
      this.usuarios = new MatTableDataSource(usuarios)
    });
  }
}
