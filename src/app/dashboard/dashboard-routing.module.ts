import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from '../auth.guard';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'checklist',
        loadChildren: '../check-list/check-list.module#CheckListModule',
      },
      {
        path: 'imagem',
        loadChildren: '../imagem/imagem.module#ImagemModule',
      },
      {
        path: 'usuario',
        loadChildren: '../usuario/usuario.module#UsuarioModule',
      },
      {
        path: 'voucher',
        loadChildren: '../voucher/voucher.module#VoucherModule',
      },
      {
        path: 'wishlist',
        loadChildren: '../wish-list/wish-list.module#WishListModule',
      },
      {
        path: 'programacao',
        loadChildren: '../programacao/programacao.module#ProgramacaoModule',
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
