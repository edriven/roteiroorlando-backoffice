import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from './material/material.module';
import { ServicosModule } from './http/servicos.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    ServicosModule.forRoot()
  ],
  declarations: [],
  exports: [
    CommonModule,
    FormsModule,
    MaterialModule
  ]
})
export class SharedModule { }
