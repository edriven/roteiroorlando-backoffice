import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { ProgramacaoService } from '../../http/programacao.service';

@Component({
  selector: 'app-grid-programacao',
  templateUrl: './grid-programacao.component.html',
  styleUrls: ['./grid-programacao.component.css']
})
export class GridProgramacaoComponent implements OnInit {
  public colunas = ['nome', 'email'];
  public usuarios = new MatTableDataSource();

  constructor(
    private programacaoService: ProgramacaoService
  ) { }

  ngOnInit() {
    this.programacaoService.listar().subscribe(usuarios =>{
      this.usuarios = new MatTableDataSource(usuarios)
    });
  }

  applyFilter(filterValue: string) {
    this.usuarios.filter = filterValue.trim().toLowerCase();
  }
}
