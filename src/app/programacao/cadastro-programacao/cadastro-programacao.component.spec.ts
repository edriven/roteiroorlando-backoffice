import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroProgramacaoComponent } from './cadastro-programacao.component';

describe('CadastroProgramacaoComponent', () => {
  let component: CadastroProgramacaoComponent;
  let fixture: ComponentFixture<CadastroProgramacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroProgramacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroProgramacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
