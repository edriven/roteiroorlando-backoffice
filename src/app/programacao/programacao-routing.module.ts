import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProgramacaoComponent } from './programacao/programacao.component';
import { CadastroProgramacaoComponent } from './cadastro-programacao/cadastro-programacao.component';

const routes: Routes = [
  {
    path: '',
    component: ProgramacaoComponent,
  },
  {
    path: 'cadastro',
    component: CadastroProgramacaoComponent,
  },
  {
    path: 'cadastro/:id',
    component: CadastroProgramacaoComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgramacaoRoutingModule { }
