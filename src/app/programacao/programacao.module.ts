import { NgModule } from '@angular/core';

import { SharedModule } from '../shared.module';
import { ProgramacaoRoutingModule } from './programacao-routing.module';
import { ProgramacaoComponent } from './programacao/programacao.component';
import { CadastroProgramacaoComponent } from './cadastro-programacao/cadastro-programacao.component';
import { GridProgramacaoComponent } from './grid-programacao/grid-programacao.component';

@NgModule({
  imports: [
    SharedModule,
    ProgramacaoRoutingModule
  ],
  declarations: [ProgramacaoComponent, CadastroProgramacaoComponent, GridProgramacaoComponent]
})
export class ProgramacaoModule { }
