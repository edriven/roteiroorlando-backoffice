import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material';
import { ChecklistService } from '../../http/checklist.service';

@Component({
  selector: 'app-grid-check-list',
  templateUrl: './grid-check-list.component.html',
  styleUrls: ['./grid-check-list.component.css']
})
export class GridCheckListComponent implements OnInit {

  public checkLists = new MatTableDataSource();
  public colunas = ['roteiro'];

  constructor(
    private checklistService: ChecklistService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.checklistService.listar().subscribe(checkLists =>{
      this.checkLists = new MatTableDataSource(checkLists);
    });
  }

  selecionarLinha(linha){
    this.router.navigate(['cadastro/'+ linha.id], {relativeTo: this.activatedRoute});
  }
}
