import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CheckListComponent } from './check-list/check-list.component';
import { CadastroCheckListComponent } from './cadastro-check-list/cadastro-check-list.component';

const routes: Routes = [
  {
    path: '',
    component: CheckListComponent,
  },
  {
    path: 'cadastro/:id',
    component: CadastroCheckListComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckListRoutingModule { }
