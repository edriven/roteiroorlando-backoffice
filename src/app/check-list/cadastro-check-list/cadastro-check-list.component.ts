import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChecklistService } from '../../http/checklist.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-cadastro-check-list',
  templateUrl: './cadastro-check-list.component.html',
  styleUrls: ['./cadastro-check-list.component.css']
})
export class CadastroCheckListComponent implements OnInit {
  
  public id: any;
  public checklist: any = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private checklistService: ChecklistService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params =>{
      this.id = params['id'];

      this.checklistService.obter(this.id).subscribe(checklist =>{
        this.checklist = checklist;
      });
    });
  }

  update(){
    let itens = this.checklist.filter(obj =>{
      return obj.task != null && obj.daysBefore != null;
    });

    this.checklistService.update(this.id, itens).then(() =>{
      this.snackBar.open('Checklist atualizado com sucesso');
      this.router.navigateByUrl('dashboard/checklist');
    });
  }

}
