import { NgModule } from '@angular/core';

import { CheckListRoutingModule } from './check-list-routing.module';
import { CheckListComponent } from './check-list/check-list.component';
import { SharedModule } from '../shared.module';
import { GridCheckListComponent } from './grid-check-list/grid-check-list.component';
import { CadastroCheckListComponent } from './cadastro-check-list/cadastro-check-list.component';

@NgModule({
  imports: [
    SharedModule,
    CheckListRoutingModule
  ],
  declarations: [CheckListComponent, GridCheckListComponent, CadastroCheckListComponent],
  exports: [SharedModule]
})
export class CheckListModule { }
